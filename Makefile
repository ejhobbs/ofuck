.PHONY: default
default: ofuck.native

test: tests.native

%.native:
	ocamlbuild -use-ocamlfind -pkg oUnit $@
	mv $@ $*
