type op = IncP | DecP | IncB | DecB | Out | In | JmpF | JmpB
val available_msg : string
val get_parser : string -> (string -> op option)
val is_available : string -> bool
