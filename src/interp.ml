module U = Util

exception Unmatched_Parentheses
exception Cell_Overrun
exception Cell_Underrun

type modifier = Inc | Dec

(* start off memory as empty *)
let data = ref (Array.make 0 0, 0)

let mov_ptr (op : modifier) : unit =
  let (bytes, pos) = !data in
  data := match op with
    | Inc -> begin
        match Array.length bytes with
        | l when l = pos+1 -> raise Cell_Overrun
        | _ -> (bytes, succ pos)
      end
    | Dec -> begin
        match pos with
        | 0 -> raise Cell_Underrun
        | p -> (bytes, p-1)
      end

let ptr_pos unit : int =
  let (_, pos) = !data in pos

let ptr_val unit : int =
  let (b, pos) = !data in
  Array.get b pos

let set_byte (f: int->int) : unit =
  let (b, p) = !data in
  let ch = Array.get b p in
  Array.set b p @@ U.wrap_int f ch

let modify_data (op: modifier) : unit =
  match op with
    | Inc -> set_byte succ
    | Dec -> set_byte pred

let get_char unit : unit =
  set_byte @@ fun _ -> int_of_char (input_char stdin)

let put_char unit : unit =
  print_char @@ char_of_int (ptr_val ())

module F = Frontend

let if_zero t f : (F.op list * F.op list) option =
  Some (match ptr_val () with
    | 0 -> t ()
    | _ -> f ())


(* fast forward over 'tape' until we get to matching end block char *)
let rec skip_block nest l r =
  match r with
  | [] -> raise Unmatched_Parentheses
  | c::rest when c = F.JmpF -> skip_block (succ nest) (c::l) rest
  | c::rest when c = F.JmpB ->
    begin
      match nest with
      | 0 -> c::l, rest
      | _ -> skip_block (pred nest) (c::l) rest
    end
  | c::rest -> skip_block nest (c::l) rest

(* Rewind over the 'tape' until we get back to the previous matching start block char *)
let rec reset_block nest l r =
  match l with
  | [] -> raise Unmatched_Parentheses
  | c::rest when c = F.JmpB -> reset_block (succ nest) rest (c::r)
  | c::rest when c = F.JmpF ->
    begin
      match nest with
      | 0 -> c::rest, r
      | _ -> reset_block (pred nest) rest (c::r)
    end
  | c::rest -> reset_block nest rest (c::r)


let interp (size: int) (prog : F.op list) : unit =
  (* Initialise the full memory space as 0 *)
  data := Array.make size 0, 0;
  let rec doit prev next =
    let more = match next with
    | F.IncP::next' -> mov_ptr Inc; Some (F.IncP::prev, next')
    | F.DecP::next' -> mov_ptr Dec; Some (F.DecP::prev, next')
    | F.IncB::next' -> modify_data Inc; Some (F.IncB::prev, next')
    | F.DecB::next' -> modify_data Dec; Some (F.DecB::prev, next')
    | F.Out::next'  -> put_char (); Some (F.Out::prev, next')
    | F.In::next'   -> get_char (); Some (F.In::prev, next')
    | F.JmpF::next' -> if_zero
                          (fun _ -> skip_block 0 (F.JmpF::prev) next')
                          (fun _ -> (F.JmpF::prev), next')
    | F.JmpB::next' -> if_zero
                          (fun _ -> (F.JmpB::prev), next')
                          (fun _ -> reset_block 0 prev (F.JmpB::next'))
    | [] -> None in
    match more with
    | None -> ()
    | Some (p, n) -> doit p n
  in doit [] prog
