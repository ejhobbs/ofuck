let usage = "OFuck, the OCaml Brainfuck interpreter\nAvailable Parsers:\n\t" ^ Frontend.available_msg ^ "\n"

let sourcefile  = ref None
let use_stdin   = ref false
let mem_size    = ref 30000
let parser      = ref ""

let set_sourcefile (f : string) : unit =
  match !sourcefile with
  | None -> sourcefile := Some f
  | Some f' -> Format.printf "Error: Multiple source files given: %s %s\n" f f'; exit 1

let set_parser (p : string) : unit =
  match Frontend.is_available p with
  | true -> parser := p
  | _ -> Format.printf "Error: Unknown parser '%s'\n" p; exit 1

let args = [
  ("-i", Arg.Unit (fun _ -> use_stdin := !use_stdin || true), "read from stdin");
  ("-s", Arg.Int (fun s -> mem_size := s), "Number of cells");
  ("-f", Arg.String set_sourcefile, "source filename");
  ("-p", Arg.String set_parser, "Parser to use, defaults is standard brainfuck")]

let parse_args = Arg.parse args print_endline usage

module U = Util
module I = Interp

let _ : unit =
  let parser = Frontend.get_parser !parser in
  match !sourcefile, !use_stdin with
  | Some f, false -> I.interp !mem_size (U.get_prog parser (open_in f))
  | None, true -> I.interp !mem_size (U.get_prog parser stdin)
  | _ -> Arg.usage args (usage ^ "Unable to determine input method, please specify")
