val get_prog : (string -> Frontend.op option) -> in_channel -> Frontend.op list
val wrap_int : (int -> int) -> int -> int
