type op = IncP | DecP | IncB | DecB | Out | In | JmpF | JmpB

let available_parsers = ["standard"]

let default (op : string) : op option =
  match op with
  | ">" -> Some IncP
  | "<" -> Some DecP
  | "+" -> Some IncB
  | "-" -> Some DecB
  | "." -> Some Out
  | "," -> Some In
  | "[" -> Some JmpF
  | "]" -> Some JmpB
  | _ -> None

let get_parser (lang: string) : (string -> op option) =
  match lang with
  | _ -> default

let is_available p = List.mem p available_parsers

let available_msg = String.concat " " available_parsers
