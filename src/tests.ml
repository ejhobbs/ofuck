open OUnit2
module U = Util
let rec assert_in_range s m =
  match s with
  | i when i = m+1 -> ()
  | i -> assert_equal i @@ U.wrap_int (fun i -> i) i; assert_in_range (i+1) m

let assert_all _ =
  assert_in_range 0 255

let assert_edge _ =
  assert_equal 0 @@ U.wrap_int (succ) 255;
  assert_equal 255 @@ U.wrap_int (pred) 0

let util_suite =
  "Util Tests" >:::
  [ "wrap_int_edge" >:: assert_edge;
    "wrap_int_inrange" >:: assert_all ]

let _ =
  run_test_tt_main util_suite
