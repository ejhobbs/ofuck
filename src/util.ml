let rec get_prog (p: string -> Frontend.op option) (f : in_channel) : Frontend.op list =
  try
    let nxt = really_input_string f 1 in
    match p nxt with
      | Some c -> c::get_prog p f
      | None -> get_prog p f
  with End_of_file -> close_in f; []

let wrap_int (f: int->int) (i: int) : int =
  let i' = f i in
  match i' with
  | i' when i' < 0 -> 255
  | i' when i' > 255 -> 0
  | i' -> i'
